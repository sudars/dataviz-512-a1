'use strict';
/*global d3*/
/**
 * This should plot some data for the Burtin bacteria example.
 */

// This is set up based on this tutorial:
// http://www.jeromecukier.net/blog/2012/05/28/manipulating-data-like-a-boss-with-d3/

var width = 1500,
    height = 700,
    margin = 200;

var yMargin = 100;
var xMargin = 80;
var penicilinWiggle = 40;

var scale = 10;

// These will be the labels from the csv.
var csvBacteria = 'Bacteria ';
var csvGramStaining = 'Gram Staining ';
var csvNegative = 'negative';
var csvPositive = 'positive';
var csvPenicilin = 'Penicilin';

var svg = d3.select('body').append('svg')
    .attr('width', width)
    .attr('height', height)
    .attr('transform', 'translate( 200, 200)');

/**
 * Get the radius so that a circle has the given area.
 */
function getRadius(area) {
    return Math.sqrt(area / Math.PI);
}

d3.csv('../resources/dataset_a1-burtin.csv', function (csv) {
    
    var drugNames = [];
    var titleRow = d3.keys(csv[0]);
    for (var i = 1; i < titleRow.length - 1; i++) {
        drugNames.push(titleRow[i]);
    }
        
    // And now we need to make the data usable for the for each format
    // we need. There will be a point for every internal row. Thus we're
    // going to want essentially {bacteria, val, drug}. We then might take
    // a second point to account for gram staining.
    var values = [];
    var gramValues = {};
    // we'll get the names of all the bacteria, which we are going to use for
    // the x axis.
    var bacteriaNames = [];
    csv.forEach(function(d) {
        var pen = {};
        var strep = {};
        var neo = {};
        pen.drug = drugNames[0];
        strep.drug = drugNames[1];
        neo.drug = drugNames[2];

        pen.bacteria = d[csvBacteria];
        strep.bacteria = d[csvBacteria];
        neo.bacteria = d[csvBacteria];
        
        pen.val = d[pen.drug];
        strep.val = d[strep.drug];
        neo.val = d[neo.drug];

        gramValues[d[csvBacteria]] = d[csvGramStaining];

        values.push(pen);
        values.push(strep);
        values.push(neo);

        bacteriaNames.push(d[csvBacteria]);

    });

    // We're going to sort the negatives then the positives.
    var negatives = [];
    var positives = [];
    bacteriaNames.forEach(function(d) {
        if (gramValues[d] === csvNegative) {
            negatives.push(d);
        } else if (gramValues[d] === csvPositive) {
            positives.push(d);
        } else {
            console.log('unrecognized gram type: ' + d.gram);
        }
    });
    negatives = negatives.sort();
    positives = positives.sort();
    bacteriaNames = negatives.concat(positives);

    // And now we're going to order the drugs in the order as they appear in
    // bacteriaNames so that we we account for the penicilin wiggle
    // appropriately.
    var orderedValues = [];
    bacteriaNames.forEach(function(name) {
        values.forEach(function(val) {
            if (val.bacteria === name) {
                orderedValues.push(val);
            }
        });
    });
    // now we want to use those new values.
    values = orderedValues;

    var xScale = d3.scale.ordinal().domain(bacteriaNames)
        .rangePoints([margin + xMargin, width - margin]);
    // Now let's get the y axis, which we're going to use for bacteria
    // names. We're going to assume that the first column is the bacteria
    // name, and that the last one is gram staining.
    
    var yScale = d3.scale.ordinal().domain(drugNames)
        .rangePoints([height - yMargin - margin, margin]);
    var colorScale = d3.scale.category10().domain(drugNames);

    var xAxis = d3.svg.axis()
        .scale(xScale)
        .orient('bottom');
    var yAxis = d3.svg.axis()
        .scale(yScale)
        .orient('left');

    // The x axis rotation stuff came from:
    // http://www.d3noob.org/2013/01/how-to-rotate-text-labels-for-x-axis-of.html
    svg.append('g')
        .attr('class', 'axis')
        .attr('transform',
                'translate(' + (0) + ',' + (height - margin) + ')')
        .call(xAxis)
        .selectAll('text')
        .style('fill', function(d) {
            if (gramValues[d] === csvPositive) {
                return d3.hsl('red');
            } else {
                return d3.hsl('blue');
            }
        })
        .style('text-anchor', 'end')
        .attr('dx', '-.8em')
        .attr('dy', '0.15em')
        .attr('transform', function() { return 'rotate(-65)'; });

    svg.append('g')
        .attr('class', 'y-axis')
        .attr('transform', 'translate(' + margin + ', 0)')
        .call(yAxis);

    var numberOfPenSeen = 0;
    svg.selectAll('circle').data(values).enter()
        .append('circle')
        .attr('cx', function(d) { return xScale(d.bacteria); })
        .attr('cy', function(d) {
            // we're going to special case penicilin because there are some
            // really big ones.
            if (d.drug === csvPenicilin) {
                var modifier;
                if (numberOfPenSeen % 2 === 0) {
                    modifier = penicilinWiggle;
                } else {
                    modifier = -1.0 * penicilinWiggle;
                }
                numberOfPenSeen++;
                return yScale(d.drug) + modifier;
            } else {
                return yScale(d.drug);
            }
        })
        .attr('r', function(d) { return getRadius(d.val * scale); })
        .style('opacity', function(d) {
            if (d.drug === csvPenicilin) {
                return 0.5;
            } else {
                return 0.8;
            }
        })
        .style('fill', function(d) { return colorScale(d.drug); });

    // This plots just the values for A. aerogenes, which is on the left side,
    // and thus we are going to use it to provide scale.
    var leftValues = [];
    values.forEach(function(d) {
        if (d.bacteria === 'A. aerogenes') {
            leftValues.push(d);
        }
    });
    svg.selectAll('text.label').append('text').data(leftValues).enter()
        .append('text')
        .attr('font-size', 12)
        .attr('fill', d3.hsl('gray'))
        .attr('x', function(d) {
            // We'll have to special case penicilin, which is too big for the
            // standard offset we're doing.
            if (d.drug === csvPenicilin) {
                return xScale(d.bacteria) + 40;
            } else {
                return xScale(d.bacteria) + 6;
            }
        })
        .attr('y', function(d) {
            if (d.drug === csvPenicilin) {
                return yScale(d.drug);
            } else {
                return yScale(d.drug);
            }
        })
        .text(function(d) { return d.val; });

    // And now we'll add a couple labels for positive and negative
    svg.append('text')
        .attr('class', 'label')
        .attr('x', 150)
        .attr('y', 540)
        //.attr('font-family', 'sans-serif')
        //.attr('font-size', 18)
        .attr('fill', d3.hsl('blue'))
        .text('Gram -');

    svg.append('text')
        .attr('class', 'label')
        .attr('x', 150)
        .attr('y', 560)
        //.attr('font-family', 'sans-serif')
        //.attr('font-size', 18)
        .attr('fill', d3.hsl('red'))
        .text('Gram +');

    // And now a title
    svg.append('text')
        .attr('class', 'title')
        .attr('x', 800)
        .attr('y', 100)
        .attr('font-size', 25)
        .style('text-anchor', 'middle')
        .text('Extreme Difference of MIC, as Shown by Area');

});
